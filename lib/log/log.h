#ifndef LOG_H
#define LOG_H

#ifdef ENV_DEV
#define LOG(s) (Serial.println(s))
#define LOG_(s) (Serial.print(s))
#define LOGF(f_, ...) (Serial.printf((f_), __VA_ARGS__))
#else
#define LOG(s) 
#define LOG_(s) 
#define LOGF(f_, ...)
#endif

#endif