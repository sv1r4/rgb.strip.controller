#ifndef SERICOM_H
#define SERICOM_H

#include <Arduino.h>
#include <Stream.h>

#ifndef SWS_PACKET_SIZE
#define SWS_PACKET_SIZE 8
#endif

#ifdef LOG_SWS
#define SWS_LOG(s) (Serial.println(s))
#define SWS_LOG_(s) (Serial.print(s))
#else
#define SWS_LOG(s) 
#define SWS_LOG_(s) 
#endif

#ifndef PACKET_MAGIC
#define PACKET_MAGIC 0xF7
#endif
//0 - magic - 0xF7
//1 - mode;
//2 - r;
//3 - g;
//4 - b;
//5 - speed1,
//6 - speed2,
//7 - crc


struct LedCommand //total ~164 bytes
{
  unsigned char mode;
  unsigned char r;
  unsigned char g;
  unsigned char b;
  uint16_t speed;
};



unsigned char CRC8(const unsigned char* buf, int len){
    int crc = 0;
    for (int i = 1; i < len; i++){
      crc += buf[i];
    }
    crc = 255 - crc;
    crc++;
    return crc;
}


inline void SendCommand(Stream& serial, const LedCommand &cmd){
   unsigned char package[SWS_PACKET_SIZE];
   package[0] = PACKET_MAGIC;
   package[1] = cmd.mode;
   package[2] = cmd.r;
   package[3] = cmd.g;
   package[4] = cmd.b;   
   unsigned char speed0 = cmd.speed >> 0;
   unsigned char speed1 = cmd.speed >> 8;
   package[5] = speed0;
   package[6] = speed1;
   package[7] = CRC8(package, SWS_PACKET_SIZE-1);

   SWS_LOG_(F("Send package"));
   for(unsigned char i=0;i<SWS_PACKET_SIZE;i++){
      serial.write(package[i]);
      SWS_LOG_(package[i]);
      SWS_LOG_(F(" "));
   }  
   SWS_LOG(F(""));
   
}

inline bool ReceiveCommand(Stream& serial, LedCommand *cmd){
   
   unsigned char package[SWS_PACKET_SIZE];
   if (serial.available())
   {
      unsigned char b = serial.read();      
      SWS_LOG_("received byte ");
      SWS_LOG(b);
      if(b == PACKET_MAGIC){
         package[0] = b;
         SWS_LOG("Got magic byte");  
         auto n = serial.readBytes(&package[1], SWS_PACKET_SIZE-1);
         SWS_LOG_("Got package n="); SWS_LOG(n);  
      }           
   }

   SWS_LOG_(F("PACKAGE:"));
   for(int i =0 ;i<SWS_PACKET_SIZE;i++){
      SWS_LOG_(package[i]);SWS_LOG_(F(" "));
   }
   SWS_LOG(F(""));
  
   auto crcActual = package[SWS_PACKET_SIZE-1];
   auto crcExpected = CRC8(package, SWS_PACKET_SIZE - 1);
   if(crcActual != crcExpected){
      SWS_LOG_("Error receive command CRC error ");
      SWS_LOG_(crcActual);SWS_LOG_("!=");SWS_LOG(crcExpected);
      return false;
   }
   
   cmd->mode = package[1];
   cmd->r = package[2];
   cmd->g = package[3];
   cmd->b = package[4];
   unsigned char speed0 = package[5];
   unsigned char speed1 = package[6];
   cmd->speed = uint16_t(package[6]<<8 | package[5]);
   SWS_LOG("Success got command");

   return true;
   
}



#endif
