﻿using System;
using System.Threading.Tasks;
using led.control.communication.abstractions.dto;

namespace led.control.communication.abstractions
{
    public interface ILedStripClient:IDisposable
    {
        Task SendCommandAsync(LedCommand ledCommand);
    }
}
