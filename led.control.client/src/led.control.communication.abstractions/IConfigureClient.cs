﻿using System.Threading.Tasks;
using led.control.communication.abstractions.dto;

namespace led.control.communication.abstractions
{
    public interface IConfigureClient
    {
        Task SendConfigAsync(WiFiConfig config);
    }
}
