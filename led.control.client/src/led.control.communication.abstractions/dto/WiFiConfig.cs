﻿using Newtonsoft.Json;

namespace led.control.communication.abstractions.dto
{
    public  class WiFiConfig
    {
        [JsonProperty("wifiSsid")]
        public string Ssid { get; set; }
        [JsonProperty("wifiPass")]
        public string Password { get; set; }
    }
}
