﻿using Newtonsoft.Json;

namespace led.control.communication.abstractions.dto
{
    public class LedCommand
    {
        [JsonProperty("mode")]
        public LedMode Mode { get; set; }
        [JsonProperty("r")]
        public byte R { get; set; }
        [JsonProperty("g")]
        public byte G { get; set; }
        [JsonProperty("b")]
        public byte B { get; set; }
        [JsonProperty("speed")]
        public int Speed { get; set; }

        public static LedCommand Off => new LedCommand
        {
            B = 0,
            G = 0,
            R = 0,
            Mode = LedMode.Color,
            Speed = 500
        };

        public static LedCommand Rainbow => new LedCommand
        {
            Mode = LedMode.Rainbow,
            Speed = 700
        };

        public static LedCommand GetRainbow(int speed)
        {
            return new LedCommand
            {
                Mode = LedMode.Rainbow,
                Speed = speed
            };
        }
    }

    public enum LedMode
    {
        Rainbow = 1,
        Color = 0
    }
}
