﻿using led.control.communication.abstractions.dto;

namespace led.control.client.android.services
{
    public interface ILedModePersistence
    {
        void StoreLedMode(LedMode mode);

        LedMode GetCurrentLedMode();
    }
}