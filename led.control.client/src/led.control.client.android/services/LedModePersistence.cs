﻿using led.control.client.android.constants;
using led.control.communication.abstractions.dto;
using Xamarin.Essentials;

namespace led.control.client.android.services
{
    public class PreferencesLedModePersistence:ILedModePersistence
    {
        public void StoreLedMode(LedMode mode)
        {
            Preferences.Set(AppPreferences.CurrentLedMode, ((int) mode).ToString());
        }

        public LedMode GetCurrentLedMode()
        {
            return (LedMode)int.Parse(Preferences.Get(AppPreferences.CurrentLedMode, ((int) LedMode.Color).ToString()));
        }

    }
}