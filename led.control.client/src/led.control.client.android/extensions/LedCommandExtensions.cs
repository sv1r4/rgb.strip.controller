﻿using Android.Graphics;
using led.control.communication.abstractions.dto;

namespace led.control.client.android.extensions
{
    public static class LedCommandExtensions
    {
        public static LedCommand ToLedCommand(this int argb)
        {
            var color = new Color(argb);
            return new LedCommand
            {
                B = color.B,
                G = color.G,
                R = color.R,
                Mode = LedMode.Color,
                Speed = 50
            };

        }
    }
}