﻿using System;
using Android.App;
using Android.Runtime;
using Android.Util;
using led.control.client.android.constants;
using led.control.client.android.services;
using led.control.communication.abstractions;
using led.control.communication.ws;
using led.control.communication.ws.config;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Preferences.Android.extensions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Xamarin.Essentials;

namespace led.control.client.android
{

   [Application]
    public class App : Application
    {
        private static readonly string TAG = "App";
        private static readonly string AppCenterSecret = "a54b2230-0b5c-45d3-be55-ab46f1b9a1c5";
        public App(IntPtr handle, JniHandleOwnership transfer) : base(handle, transfer)
        {
 
        }
 
        public override void OnCreate()
        {
            InitializeServices();

            InitAppCenter();
 
            base.OnCreate(); 
        }

        private static void InitAppCenter()
        {
            AppCenter.Start(AppCenterSecret,
                typeof(Analytics), typeof(Crashes));
        }
 
        public static IServiceProvider ServiceProvider { get; set; }
 
        private static void InitializeServices()
        {
            var config = new ConfigurationBuilder()
                .AddAppPreferences()
                .Build();

            var services = new ServiceCollection();
            services.AddOptions();

            ConfigOptions<WsLedStripClientConfig>(services, config);
            ConfigOptions<WsConfigureClientConfig>(services, config);
            services.AddTransient<ILedStripClient, WsLedStripClient>();
            services.AddTransient<IConfigureClient, WsConfigureClient>();
            services.AddSingleton<ILedModePersistence, PreferencesLedModePersistence>();
            //ConfigOptions<AppConfig>(services, config);

            ServiceProvider = services.BuildServiceProvider();
            
            LogOptionsChange<WsLedStripClientConfig>(ServiceProvider);
            LogOptionsChange<WsConfigureClientConfig>(ServiceProvider);


            InitPreferenceIfNotSet(AppPreferences.WsLedStripConfigUri, "ws://192.168.1.1:81/cmd");
            InitPreferenceIfNotSet(AppPreferences.WsLedStripConfigUri, "ws://192.168.4.1:81/config");
        }

        private static void InitPreferenceIfNotSet(string key, string value)
        {
            if (!Preferences.ContainsKey(key)
                || string.IsNullOrWhiteSpace(Preferences.Get(key, string.Empty)))
            {
                Preferences.Set(key, value);
            }
        }

        private static void ConfigOptions<T>(IServiceCollection services, IConfiguration config) where T : class, new()
        {
            services.Configure<T>(config.GetSection(typeof(T).Name));
            services.AddSingleton<IOptionsMonitor<T>, OptionsMonitor<T>>();
        }

        private static void LogOptionsChange<T>(IServiceProvider serviceProvider) where T : class, new()
        {
            var name = typeof(T).Name;
            serviceProvider.GetRequiredService<IOptionsMonitor<T>>().OnChange(
                v => { Log.Debug(TAG, $"{name} changed"); });
        }
    }
}