﻿using led.control.communication.ws.config;

namespace led.control.client.android.constants
{
    public static class AppPreferences
    {
        public static readonly string WsLedStripConfigUri = $"{nameof(WsLedStripClientConfig)}:{nameof(WsLedStripClientConfig.Uri)}";
        public static readonly string WsConfigureClientConfigUri = $"{nameof(WsConfigureClientConfig)}:{nameof(WsConfigureClientConfig.Uri)}";
        public static readonly string SpeedValue = "SpeedValue";
        public static readonly string CurrentLedMode = "CurrentLedMode";
    }
}