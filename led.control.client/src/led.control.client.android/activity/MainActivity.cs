﻿using Android.App;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using led.control.client.android.constants;
using led.control.client.android.extensions;
using led.control.client.android.services;
using led.control.communication.abstractions;
using led.control.communication.abstractions.dto;
using Microsoft.Extensions.DependencyInjection;
using Xamarin.Essentials;

namespace led.control.client.android.activity
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true, Icon = "@mipmap/ic_main")]
    public class MainActivity : BaseActivity
    {
        private ILedStripClient _ledStripClient;
        private Bitmap _bmpColorPicker;
        private TextView _captionSelectedColor;
        private SeekBar _speedValue;
        private TextView _speedValueText;
        private const int DefaultSpeed = 600;
        private ILedModePersistence _ledModePersistence;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            _ledModePersistence = App.ServiceProvider.GetRequiredService<ILedModePersistence>();

            Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            var btnOff = FindViewById<Button>(Resource.Id.btnOff);
            var btnBlue = FindViewById<Button>(Resource.Id.btnBlue);
            var btnRainbow = FindViewById<Button>(Resource.Id.btnRainbow);
            var btnSettings = FindViewById<ImageButton>(Resource.Id.btnSettings);
            var imgColorPicker = FindViewById<ImageView>(Resource.Id.imgColorPicker);
            _speedValue = FindViewById<SeekBar>(Resource.Id.speedValue);
            _speedValueText = FindViewById<TextView>(Resource.Id.speedValueText);
            _captionSelectedColor = FindViewById<TextView>(Resource.Id.captionSelectedColor);

            imgColorPicker.Touch+=ImgColorPickerOnTouch;
            _bmpColorPicker = ((BitmapDrawable)imgColorPicker.Drawable).Bitmap;

            btnSettings.Click += (sender, args) => { StartActivity(typeof(SettingsActivity)); };

            btnBlue.Click += async (sender, args) =>
            {
                await TrackAsync(async () =>
                {
                    await _ledStripClient.SendCommandAsync(new LedCommand
                    {
                        B = 255,
                        Mode = LedMode.Color,
                        Speed = 500
                    });
                    _ledModePersistence.StoreLedMode(LedMode.Color);
                });
                
            };

            btnOff.Click += async (sender, args) =>
            {
                await TrackAsync(async () =>
                {
                    await _ledStripClient.SendCommandAsync(LedCommand.Off);
                    _ledModePersistence.StoreLedMode(LedMode.Color);
                });
            };
            btnRainbow.Click += async (sender, args) =>
            {
                await TrackAsync(async () =>
                {
                    await _ledStripClient.SendCommandAsync(LedCommand.GetRainbow(_speedValue.Progress));
                    _ledModePersistence.StoreLedMode(LedMode.Rainbow);
                });
            };
            var speed = int.Parse(Preferences.Get(AppPreferences.SpeedValue, DefaultSpeed.ToString()));
            _speedValue.Progress = speed;
            _speedValueText.Text = speed.ToString();
            _speedValue.ProgressChanged+=SpeedValueOnProgressChanged;
        }

     

        private async void SpeedValueOnProgressChanged(object sender, SeekBar.ProgressChangedEventArgs e)
        {
            Preferences.Set(AppPreferences.SpeedValue, e.Progress.ToString());
            _speedValueText.Text = e.Progress.ToString();
            if (_ledModePersistence.GetCurrentLedMode() == LedMode.Rainbow)
            {
                await TrackAsync(async () =>
                {
                    await _ledStripClient.SendCommandAsync(LedCommand.GetRainbow(_speedValue.Progress));
                    _ledModePersistence.StoreLedMode(LedMode.Rainbow);
                });
            }

        }


        private async void ImgColorPickerOnTouch(object sender, View.TouchEventArgs e)
        {
            
            var v = (ImageView) sender;
            var screenX = e.Event.GetX();
            var screenY = e.Event.GetY();
            var x = (int)((screenX /* - v.Left*/) * _bmpColorPicker.Width / v.Width);
            var y = (int)((screenY /*- v.Top*/) * _bmpColorPicker.Height / v.Height);

            if (x < 0) { x = 0; }
            if (x >= _bmpColorPicker.Width) { x = _bmpColorPicker.Width-1; }
            if (y < 0) { y = 0; }
            if (y >= _bmpColorPicker.Height) { y = _bmpColorPicker.Height-1; }
            var color = _bmpColorPicker.GetPixel(x,y);

            if (color == 0 || color == 0xFFFFFF) { return; }
            _captionSelectedColor.Text = $"#{color:X}";
            _captionSelectedColor.SetBackgroundColor(new Color(color));

            await TrackAsync(async () =>
            {
                await _ledStripClient.SendCommandAsync(color.ToLedCommand());
                _ledModePersistence.StoreLedMode(LedMode.Color);
            });
        }


        protected override void OnResume()
        {
            base.OnResume();
            _ledStripClient = App.ServiceProvider.GetRequiredService<ILedStripClient>();
        }

        protected override void OnDestroy()
        {
            _ledStripClient?.Dispose();
            base.OnDestroy();
        }
        

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
	}
}

