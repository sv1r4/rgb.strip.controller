﻿using Android.App;
using Android.OS;
using Android.Widget;
using led.control.client.android.constants;
using led.control.communication.abstractions;
using led.control.communication.abstractions.dto;
using Microsoft.Extensions.DependencyInjection;
using Xamarin.Essentials;

namespace led.control.client.android.activity
{
    [Activity(Label = "SettingsActivity")]
    public class SettingsActivity : BaseActivity
    {
        private IConfigureClient _configureClient;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_settings);

            _configureClient = App.ServiceProvider.GetRequiredService<IConfigureClient>();

            var btnApplyConfig = FindViewById<Button>(Resource.Id.btnApplyConfig);
            var wifiSsid = FindViewById<EditText>(Resource.Id.wifiSsid);
            var wifiPassword = FindViewById<EditText>(Resource.Id.wifiPassword);
            var wsLedStripConfigUri = FindViewById<EditText>(Resource.Id.wsLedStripConfigUri);
            var wsConfigureClientConfigUri = FindViewById<EditText>(Resource.Id.wsConfigureClientConfigUri);

            wsLedStripConfigUri.Text = Preferences.Get(AppPreferences.WsLedStripConfigUri, string.Empty);
            wsLedStripConfigUri.TextChanged += (sender, args) =>
            {
                Preferences.Set(AppPreferences.WsLedStripConfigUri, args.Text.ToString());
            };

            wsConfigureClientConfigUri.Text = Preferences.Get(AppPreferences.WsConfigureClientConfigUri, string.Empty);
            wsConfigureClientConfigUri.TextChanged += (sender, args) =>
            {
                Preferences.Set(AppPreferences.WsConfigureClientConfigUri, args.Text.ToString());
            };

            btnApplyConfig.Click += async (sender, args) =>
                {
                    await TrackAsync(async () =>
                    {
                        await _configureClient.SendConfigAsync(new WiFiConfig
                        {
                            Ssid = wifiSsid.Text,
                            Password = wifiPassword.Text
                        });
                    });
            };
            
        }

    }
}