﻿using System;
using System.Threading.Tasks;
using Android.Support.V7.App;
using Android.Widget;
using Microsoft.AppCenter.Crashes;

namespace led.control.client.android.activity
{
    public abstract class BaseActivity:AppCompatActivity
    {
        protected async Task TrackAsync(Func<Task> action)
        {
            try
            {
                await action();
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex);
                Toast.MakeText(base.ApplicationContext, ex.Message, ToastLength.Short).Show();
            }
        }
    }
}