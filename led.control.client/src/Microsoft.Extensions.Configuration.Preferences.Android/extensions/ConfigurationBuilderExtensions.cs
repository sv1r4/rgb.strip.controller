﻿namespace Microsoft.Extensions.Configuration.Preferences.Android.extensions
{
    public static class ConfigurationBuilderExtensions
    {
        public static IConfigurationBuilder AddAppPreferences(this IConfigurationBuilder builder)
        {
            builder.Add(new PreferencesConfigurationSource());
            return builder;
        }
    }
}