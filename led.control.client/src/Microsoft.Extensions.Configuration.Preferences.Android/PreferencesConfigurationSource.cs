﻿namespace Microsoft.Extensions.Configuration.Preferences.Android
{
    public class PreferencesConfigurationSource:IConfigurationSource
    {
        public IConfigurationProvider Build(IConfigurationBuilder builder)
        {
            return new PreferencesConfigurationProvider();
        }
    }
}