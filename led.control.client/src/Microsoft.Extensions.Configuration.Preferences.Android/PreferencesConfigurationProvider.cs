﻿using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Content;
using Android.Preferences;

namespace Microsoft.Extensions.Configuration.Preferences.Android
{
    public class PreferencesConfigurationProvider:ConfigurationProvider
    {
        private PreferenceChangeListener _listener;
        public override void Load()
        {
            Load(false);

            _listener = new PreferenceChangeListener();
            _listener.SetProvider(this);
            PreferenceManager.GetDefaultSharedPreferences(Application.Context).RegisterOnSharedPreferenceChangeListener(_listener);
        }

        public void Load(bool reload)
        {
            if (reload)
            {
                Data = new Dictionary<string, string>();
            }
            
            foreach (var preferenceKey in PreferenceManager.GetDefaultSharedPreferences(Application.Context).All.Select(p=>p.Key))
            {
                string value = null;
                try
                {
                    value = Xamarin.Essentials.Preferences.Get(preferenceKey, null);
                }
                catch
                {
                    //ignore
                }
                Data.Add(NormalizeKey(preferenceKey), Xamarin.Essentials.Preferences.ContainsKey(preferenceKey) ? value : null);
            }
            OnReload();
        }

        private static string NormalizeKey(string key)
        {
            return key.Replace("__", ConfigurationPath.KeyDelimiter);
        }


        public class PreferenceChangeListener:Java.Lang.Object, ISharedPreferencesOnSharedPreferenceChangeListener
        {
            private PreferencesConfigurationProvider _provider;

            public void SetProvider(PreferencesConfigurationProvider provider)
            {
                _provider = provider;
            }
            public void OnSharedPreferenceChanged(ISharedPreferences sharedPreferences, string key)
            {
                _provider?.Load(true);
            }
        }
    }


}