﻿using System;
using System.Threading.Tasks;

namespace led.control.communication.ws
{
    public interface IWsClient:IDisposable
    {
        Task SendAsJsonAsync<T>(T obj);
    }
}
