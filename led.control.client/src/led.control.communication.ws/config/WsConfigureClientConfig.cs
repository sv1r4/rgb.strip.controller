﻿namespace led.control.communication.ws.config
{
    public class WsConfigureClientConfig
    {
        public string Uri { get; set; }
    }
}
