﻿namespace led.control.communication.ws.config
{
    public class WsLedStripClientConfig
    {
        public string Uri { get; set; }
    }
}
