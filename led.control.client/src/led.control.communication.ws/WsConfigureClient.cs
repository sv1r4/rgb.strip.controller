﻿using System.Threading.Tasks;
using led.control.communication.abstractions;
using led.control.communication.abstractions.dto;
using led.control.communication.ws.config;
using Microsoft.Extensions.Options;

namespace led.control.communication.ws
{
    public class WsConfigureClient:IConfigureClient
    {
        private readonly WsConfigureClientConfig _options;

        public WsConfigureClient(IOptionsMonitor<WsConfigureClientConfig> options)
        {
            _options = options.CurrentValue;
        }

        public async Task SendConfigAsync(WiFiConfig config)
        {
            using (var ws = new WsClient(_options.Uri))
            {
                await ws.SendAsJsonAsync(config);
                await Task.Delay(1500);
            }
        }
    }
}
