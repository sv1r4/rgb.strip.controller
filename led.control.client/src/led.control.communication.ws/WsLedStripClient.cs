﻿
using System.Threading.Tasks;
using led.control.communication.abstractions;
using led.control.communication.abstractions.dto;
using led.control.communication.ws.config;
using Microsoft.Extensions.Options;

namespace led.control.communication.ws
{
    public class WsLedStripClient:ILedStripClient
    {
        private readonly IWsClient _ws;

        public WsLedStripClient(IOptionsMonitor<WsLedStripClientConfig> config)
        {
            _ws = new WsClient(config.CurrentValue.Uri);
        }

        public Task SendCommandAsync(LedCommand ledCommand)
        {
            return _ws.SendAsJsonAsync(ledCommand);
        }

        public void Dispose()
        {
            _ws?.Dispose();
        }
    }
}
