﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WebSocket4Net;

namespace led.control.communication.ws
{
    public class WsClient: IWsClient
    {
        private static readonly TimeSpan ConnectTimeout = TimeSpan.FromSeconds(5);
        private static readonly TimeSpan ConnectCheckInterval = TimeSpan.FromMilliseconds(10);
        private readonly WebSocket _ws;

        public WsClient(string uri)
        {
            _ws = new WebSocket(uri){/*AutoSendPingInterval = 1000*/};
        }

        public async Task SendAsJsonAsync<T>(T obj)
        {
            if (_ws.State != WebSocketState.Open && _ws.State != WebSocketState.Connecting)
            {
                _ws.Open();
            }

            if (_ws.State == WebSocketState.Connecting)
            {
                using (var cts = new CancellationTokenSource())
                {
                    cts.CancelAfter(ConnectTimeout);
                    while (_ws.State == WebSocketState.Connecting )
                    {
                        await Task.Delay(ConnectCheckInterval, cts.Token);
                    }
                }
            }
            
            _ws.Send(JsonConvert.SerializeObject(obj));
        }

        public void Dispose()
        {
            _ws?.Dispose();
        }
    }
}
