#ifndef wifi_h
#define wifi_h

#include "config.h"
#include <ESP8266WiFi.h>
#include "log.h"

inline void wifiFlush(){
  // if (WiFi.isConnected())
  // {    
  //   WiFi.disconnect();
  // }    
  
  // WiFi.begin("flush", "flush"); //flush prev data
  // WiFi.persistent(false);
  // WiFi.setAutoReconnect(true);
  // WiFi.setAutoConnect(true);
  // WiFi.softAPdisconnect();
  WiFi.mode(WIFI_OFF);
}

inline void wifiSetSta(const Config &config){
  if (strlen(config.wifiSsid) == 0)
  {
    LOG(F("No SSID passed. skip config STA params"));
    return;
  }

  WiFi.begin(config.wifiSsid, config.wifiPass);
}

inline void wifiSetAp(const char* apSsid, const char* apPass){
  IPAddress apIP(192, 168, 4, 1);
  WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
  if(WiFi.softAP(apSsid, apPass, 1, 0, 4)){
    LOGF("Success set AP '%s' pass '%s'\n", apSsid, apPass);
  }else{
    LOGF("Fail set AP '%s' pass '%s'\n", apSsid, apPass);
  }
}

inline void wifiApSta(const Config &config, const char* apSsid, const char* apPass, const char* hostname){
  LOG(F("Wifi set mode WIFI_AP_STA"));    
  
  wifiFlush();
   
  WiFi.mode(WIFI_AP_STA);
  WiFi.hostname(hostname);
  wifiSetAp(apSsid, apPass);
  wifiSetSta(config);
  
}


inline void wifiAp(const char* apSsid, const char* apPass){
  LOG(F("Wifi set mode WIFI_AP"));    
  
  wifiFlush();
  WiFi.mode(WIFI_AP);
   
  wifiSetAp(apSsid, apPass);
}

inline void wifiSta(Config &config, const char* hostname){
  LOG(F("Wifi set mode WIFI_STA"));  
  
  //WiFi.softAPdisconnect();
  wifiFlush();

  WiFi.mode(WIFI_STA);  
  WiFi.hostname(hostname);
  wifiSetSta(config);
}


inline bool wifiWaitForConnect(unsigned long timeout_ms)
{ 
  unsigned long start = millis();
  while (millis() - start < timeout_ms)
  {
    delay(3);
    auto status = WiFi.status();

    LOG_(status);
    if (status == WL_CONNECTED)
    {
      LOG();
      LOG(F("Wifi connect success")); 
      return true;
    }
  }
  LOG(F("Wifi connection failed"));
  return false;
}


#endif