#ifndef BLINKER_RGB_H
#define BLINKER_RGB_H

#include <Arduino.h>

#ifndef SEQ_SIZE
#define SEQ_SIZE  15
#endif

#ifndef REFRESH_RATE_MS
#define REFRESH_RATE_MS 5
#endif

typedef struct Rgb
{
    float r;
    float g;
    float b;
} Rgb;

typedef struct Seq
{
    unsigned char r;
    unsigned char g;
    unsigned char b;
    uint16_t speed;//time in ms to move to this pixel value
} Seq;

class BlinkerRgb
{
    private:       
        int _pinR;
        int _pinG;
        int _pinB;
        Seq _seq[SEQ_SIZE];//array of sequence colors
        uint32_t _seqIndex;
        uint32_t _seqCnt = 0;//cnt of colors (should be less then SEQ_SIZE)                       
        Rgb _rgb; //current pixels state
        unsigned long _loopStarted = 0;
        uint16_t _maxLoopSpeed = 0;
        float _deltas[3];//current move steps for each rgb channel 
        uint32_t getC(uint32_t color, byte i);
        float moveToTarget(float c, unsigned char t, float delta);
        void init();   
        unsigned long _tLastLoop; 
        bool loopPixel();
        void calcDeltas();
        void calcMaxLoopSpeed();
        bool isPixelOk(const Rgb &rgb, const Seq &seq);
    public:    
        BlinkerRgb(int pinR, int pinG, int pinB);
        void start();             
        void loop();
        void setSeqCnt(int seqCnt);
        void setSeqIndex(uint32_t index);
        uint32_t getSeqIndex();
        uint32_t getSeqCnt();
        void setSeqColor(uint16_t index, Seq seqItem);

};


#endif
