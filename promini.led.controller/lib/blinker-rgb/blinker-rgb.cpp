#include "blinker-rgb.h"



BlinkerRgb::BlinkerRgb(int pinR, int pinG, int pinB) : 
 _pinR(pinR),
 _pinG(pinG),
 _pinB(pinB)
{
    init();
}

void BlinkerRgb::init()
{
    
}



void BlinkerRgb::start()
{
    pinMode(_pinR, OUTPUT);
    pinMode(_pinG, OUTPUT);
    pinMode(_pinB, OUTPUT);

    
    _loopStarted = 0;
    calcDeltas();
}


void BlinkerRgb::calcDeltas(){   
   
        float iters = ((float)_seq[_seqIndex].speed / REFRESH_RATE_MS);
        
        float distR = ((float) abs((int)_seq[_seqIndex].r - (int)_rgb.r));
        
        _deltas[0] = iters>0 ? distR/iters : 0;
        
        float distG = ((float) abs((int)_seq[_seqIndex].g - (int)_rgb.g));
        
        _deltas[1] = iters>0 ? distG/iters : 0;
        
        float distB =((float) abs((int)_seq[_seqIndex].b - (int)_rgb.b));
        
        _deltas[2] = iters>0 ? distB/iters : 0;  
}

void BlinkerRgb::loop(){
    unsigned long now = millis();
    if(now-_tLastLoop > REFRESH_RATE_MS){
        _tLastLoop = now;
    }else{
        return;
    }

    bool done = loopPixel();
   
    if((now - _loopStarted >= _maxLoopSpeed) 
        && done){
      //  Serial.print("done seqindex = "); Serial.println(_seqIndex);   
        _seqIndex++; 
        if (_seqIndex >= _seqCnt)
        { 
            _seqIndex = 0;                
        }    
        _loopStarted = now;
        calcMaxLoopSpeed();
        calcDeltas();
    }
    
}


void BlinkerRgb::calcMaxLoopSpeed(){
    _maxLoopSpeed = _seq[_seqIndex].speed;
}

void BlinkerRgb::setSeqCnt(int seqCnt)
{
    if (seqCnt > SEQ_SIZE || seqCnt < 1)
    {
        return;
    }
    _seqCnt = seqCnt;
    
    calcDeltas();
}


void BlinkerRgb::setSeqIndex(uint32_t index)
{
    if (index >= _seqCnt || index < 0)
    {
        return;
    }
    _seqIndex = index;
    
    calcDeltas();
}



uint32_t BlinkerRgb::getSeqIndex()
{
    return _seqIndex;
}


uint32_t BlinkerRgb::getSeqCnt()
{
    return _seqCnt;
}


void BlinkerRgb::setSeqColor(uint16_t index, Seq seqItem)
{
    if (index >= _seqCnt || index < 0)
    {        
        return;
    }
    _seq[index] = seqItem;
    
    calcDeltas();
}

bool BlinkerRgb::isPixelOk(const Rgb &rgb, const Seq &seq){
    
    bool ok = abs(rgb.r - seq.r) < 1.0
        && abs(rgb.g - seq.g ) < 1.0
        && abs(rgb.b - seq.b ) < 1.0;

    return ok;
}

bool BlinkerRgb::loopPixel()
{    
    auto seq = _seq[_seqIndex];
    
    
    if(isPixelOk(_rgb, seq)){
        return true;
    }
    _rgb.r = moveToTarget(_rgb.r, seq.r, _deltas[0]);
    _rgb.g = moveToTarget(_rgb.g, seq.g, _deltas[1]);
    _rgb.b = moveToTarget(_rgb.b, seq.b, _deltas[2]);

    //Serial.print("_rgb.r ");Serial.print(_rgb.r);Serial.print(" ");Serial.println((uint8_t)_rgb.r);    
    analogWrite(_pinR, (uint8_t)_rgb.r);
    analogWrite(_pinG, (uint8_t)_rgb.g);
    analogWrite(_pinB, (uint8_t)_rgb.b);

    if(isPixelOk(_rgb, seq)){
        return true;
    }

    return false;
}

#pragma region util methods


float BlinkerRgb::moveToTarget(float c, unsigned char t, float delta)
{    
    if (abs(c - t)<1.0)
    {
        return t;
    }
    if(abs(c - t) <= delta){
       return t;
    }
    if (c < t)
    {
        return c + delta;
    }
    return c - delta;
}

#pragma endregion