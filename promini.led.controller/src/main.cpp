#include <Arduino.h>
#include <SoftwareSerial.h>


#define SWS_LOGF
#include "log.h"
#include "sericom.h"
#include "blinker-rgb.h"

#define PIN_SWS_RX 11
#define PIN_SWS_TX 10
#define SWS_SPEED 9600


SoftwareSerial sws(PIN_SWS_RX, PIN_SWS_TX, false);
BlinkerRgb blinker(3, 6, 5);

void setup() {
  
  Serial.begin(115200);
 
  sws.begin(SWS_SPEED);

  blinker.start();
  LOG(F("Booting"));
  // put your setup code here, to run once:
}

void setColor(const LedCommand &cmd){
  blinker.setSeqCnt(1);
  blinker.setSeqColor(0, Seq{
    cmd.r,
    cmd.g,
    cmd.b,
    cmd.speed
  });
}

void setRainbow(uint16_t speed){
  blinker.setSeqCnt(12);
  blinker.setSeqIndex(0);
  blinker.setSeqColor(0,  Seq{ 255, 0,    0, speed  });
  blinker.setSeqColor(1,  Seq{ 255, 127,  0, speed  });
  blinker.setSeqColor(2,  Seq{ 255, 255,  0, speed  });
  blinker.setSeqColor(3,  Seq{ 127, 255,  0, speed  });
  blinker.setSeqColor(4,  Seq{ 0,   255,  0, speed  });
  blinker.setSeqColor(5,  Seq{ 0,   255,  127, speed  });
  blinker.setSeqColor(6,  Seq{ 0,   255,  255, speed  });
  blinker.setSeqColor(7,  Seq{ 0,   127,  255, speed  });
  blinker.setSeqColor(8,  Seq{ 0,   0,    255, speed  });
  blinker.setSeqColor(9,  Seq{ 127, 0,    255, speed  });
  blinker.setSeqColor(10, Seq{ 255, 0,    255, speed  });
  blinker.setSeqColor(11, Seq{ 255, 0,    127, speed  });
}

void handleCommand(const LedCommand &cmd){
  if(cmd.mode == 0)//collor
  {
    LOG(F("Set color"));
    setColor(cmd);
  }else if (cmd.mode == 1)//rainbow
  {
    LOG(F("Set rainbow"));
    setRainbow(cmd.speed);
  }
}

void loop() {
  if(sws.available()){
    LedCommand cmd;
    if(ReceiveCommand(sws, &cmd)){
      LOG_(F("Got cmd "));LOG_(cmd.mode);
      LOG_(F(" "));LOG_(cmd.r);
      LOG_(F(" "));LOG_(cmd.g);
      LOG_(F(" "));LOG_(cmd.b);
      LOG_(F(" "));LOG(cmd.speed);

      handleCommand(cmd);
    }
  }
  blinker.loop();
  // put your main code here, to run repeatedly:
}